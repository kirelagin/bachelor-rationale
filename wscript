def configure(conf):
    conf.load('tex')
    conf.env.append_value('PDFLATEXFLAGS', '-interaction=nonstopmode')
    conf.find_program('pandoc')


def build(bld):
    bld(
        rule   = '${PANDOC} --template=${SRC[0].abspath()} -S --bibliography=${SRC[1].abspath()} --natbib ${SRC[2].abspath()} -o ${TGT}',
        source = ['template.latex', 'bib.bib', 'rationale.pd'],
        target = 'rationale.latex',
    )

    bld.add_group()

    bld(
        features = 'tex',
        type     = 'pdflatex',
        source   = 'rationale.latex',
        target   = 'rationale.pdf',
       )
